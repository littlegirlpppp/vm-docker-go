module chainmaker.org/chainmaker/vm-docker-go/v2/vm_mgr

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.1.0
	chainmaker.org/chainmaker/protocol/v2 v2.1.1-0.20211117024857-2641037a7269
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/golang/protobuf v1.5.2
	go.uber.org/atomic v1.7.0
	go.uber.org/zap v1.19.1
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1 // indirect
)
